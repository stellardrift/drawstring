import ca.stellardrift.build.common.adventure
import ca.stellardrift.build.common.pex
import ca.stellardrift.build.configurate.ConfigFormats
import ca.stellardrift.build.configurate.transformations.convertFormat
import net.kyori.indra.sonatypeSnapshots

plugins {
    val opinionatedVersion = "4.1"
    val kotlinVersion = "1.4.21"
    kotlin("jvm") version kotlinVersion
    id("ca.stellardrift.opinionated.fabric") version opinionatedVersion
    id("ca.stellardrift.opinionated.kotlin") version opinionatedVersion
    id("ca.stellardrift.configurate-transformations") version opinionatedVersion
    id("net.kyori.indra.publishing") version "1.2.1"
    // kotlin("kapt") version kotlinVersion
    // id("com.github.fudge.autofabric") version "1.1.1"
    id("com.github.ben-manes.versions") version "0.36.0"
}

group = "ca.stellardrift"
version = "0.1-SNAPSHOT"
description = "A grab-bag of tweaks and other features"

ktlint {
    version.set("0.40.0")
}

repositories {
    maven(url = "https://dl.bintray.com/ladysnake/libs") {
        name = "ladysnakeLibs"
    }
    pex()
    jcenter()
    sonatypeSnapshots()
}

indra {
    gitlab("stellardrift", "drawstring")
    apache2License()

    publishAllTo("pex", "https://repo.glaremasters.me/repository/permissionsex")
    // publication?.artifactId = name.toLowerCase() // TODO: Put this into opinionated publishing
}

val versionMinecraft: String by project
val versionYarn: String by project
val versionLoader: String by project
val versionFabricApi: String by project

dependencies {
    // Fabric
    minecraft("com.mojang:minecraft:$versionMinecraft")
    mappings("net.fabricmc:yarn:$versionMinecraft+build.$versionYarn:v2")
    modImplementation("net.fabricmc:fabric-loader:$versionLoader")
    modImplementation("net.fabricmc:fabric-language-kotlin:1.4.21+build.1")
    modImplementation("net.fabricmc.fabric-api:fabric-api:$versionFabricApi")

    // Configurate
    include(
        modImplementation("ca.stellardrift:confabricate:2.0.0+4.0.0") {
            exclude("com.google.code.gson")
        }
    )
    include(modImplementation("org.spongepowered:configurate-extra-kotlin:4.0.0")!!)

    include(modImplementation(adventure("platform-fabric", "4.0.0-SNAPSHOT"))!!)
    include(modImplementation(adventure("text-minimessage", "4.1.0-SNAPSHOT"))!!)
    include(modImplementation(adventure("serializer-configurate4", "4.3.0"))!!)

    // Misc libs
    modImplementation(include("io.github.ladysnake:PlayerAbilityLib:1.2.1")!!)
    modImplementation(include("ca.stellardrift:colonel:0.1")!!)

    compileOnly("org.checkerframework:checker-qual:3.8.0")
}

tasks.withType(net.fabricmc.loom.task.AbstractRunTask::class) {
    // jvmArgs("-Dmixin.debug.verbose=true", "-Dmixin.debug.export=true")
}

tasks.withType(ProcessResources::class).configureEach {
    inputs.property("version", project.version)
    filesMatching("fabric.mod.yml") {
        expand("project" to project)
        convertFormat(ConfigFormats.YAML, ConfigFormats.JSON)
        name = "fabric.mod.json"
    }

    filesMatching("*.mixins.json") {
        expand("project" to project)
    }
}
