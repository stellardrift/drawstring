# Drawstring

[![pipeline status](https://gitlab.com/stellardrift/drawstring/badges/dev/pipeline.svg)](https://gitlab.com/stellardrift/drawstring/-/commits/dev)

An incubator for various mod ideas, primarily server-focused.
Any feature in this project should be assumed to be *experimental*, and subject to change. As features mature, they will most likely be moved into their own standalone projects.

## Features

- Provides the `drawstring:module` entrypoint used to discover modules provided within other mods

## Using

We build with Gradle, and require Java 1.8.

Drawstring is released under the terms of the Apache 2.0 license.

