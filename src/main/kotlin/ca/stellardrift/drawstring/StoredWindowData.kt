/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring

import org.spongepowered.configurate.gson.GsonConfigurationLoader
import org.spongepowered.configurate.kotlin.extensions.get
import org.spongepowered.configurate.kotlin.extensions.set
import org.spongepowered.configurate.kotlin.objectMapperFactory
import org.spongepowered.configurate.loader.ConfigurationLoader
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import org.spongepowered.configurate.objectmapping.meta.Comment

interface WindowSettingsBridge {
    // starting x and y position of the window
    var startX: Int
    var startY: Int
}

@ConfigSerializable
data class StoredWindowData(
    @Comment("If true, will override any CLI arguments passed about window size")
    val agressive: Boolean = false,
    // initial width and height
    val width: Int = 854,
    val height: Int = 600,

    // initial starting position
    val startX: Int = -1,
    val startY: Int = -1
)

private fun windowDataLoader(): ConfigurationLoader<*> {
    return GsonConfigurationLoader.builder()
        .path(Drawstring.mainConfig.resolveSibling("window-data.json"))
        .defaultOptions {
            it.serializers { it.registerAnnotatedObjects(objectMapperFactory()) }
                .header(
                    """Window position data saved for client.
                |Automatically written to on client close.""".trimMargin()
                )
        }
        .build()
}

fun loadWindowData(): StoredWindowData {
    return windowDataLoader().load().get(StoredWindowData::class)!!
}

fun saveWindowData(data: StoredWindowData) {
    val loader = windowDataLoader()
    val node = loader.load()
    node.set(StoredWindowData::class, data)
    loader.save(node)
}
