/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring.module

import ca.stellardrift.drawstring.Module
import ca.stellardrift.drawstring.key
import com.mojang.brigadier.Command
import com.mojang.brigadier.arguments.StringArgumentType.getString
import com.mojang.brigadier.arguments.StringArgumentType.greedyString
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents
import net.kyori.adventure.audience.MessageType
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.Component.text
import net.kyori.adventure.text.format.Style
import net.kyori.adventure.text.format.Style.style
import net.kyori.adventure.text.format.TextColor
import net.kyori.adventure.text.format.TextColor.color
import net.kyori.adventure.text.minimessage.MiniMessage
import net.minecraft.command.argument.EntityArgumentType.getPlayers
import net.minecraft.command.argument.EntityArgumentType.players
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import org.spongepowered.configurate.objectmapping.meta.Comment

private const val ARG_TARGET = "target"
private const val ARG_MESSAGE = "message"

/**
 * A module that will control game messages
 */
class GameMessages : Module() {
    override val name: String
        get() = "game-messages"

    val options: StoredMessages by config()

    val tell by command {
        then(
            players().key(ARG_TARGET).then(
                greedyString().key(ARG_MESSAGE).executes { ctx ->
                    val sender = ctx.source.audience()
                    val targets = getPlayers(ctx, ARG_TARGET)
                    val message = MiniMessage.get().parse(getString(ctx, ARG_MESSAGE))

                    targets.audience().sendMessage(sender, message, MessageType.CHAT)

                    Command.SINGLE_SUCCESS
                }
            )
        )
    }

    override fun initialize() {
        ServerPlayConnectionEvents.JOIN.register { handler, _, _ ->
            handler.player.audience().sendMessage(options.join)
        }
    }

    fun transformCommandSourceResponse(original: Component): Component {
        val prefix = options.commandPrefix
        val style = options.commandStyle
        return if (prefix != Component.empty()) {
            text().style(style)
                .append(prefix)
                .append(original)
                .build()
        } else {
            original.style(original.style().merge(style, Style.Merge.Strategy.IF_ABSENT_ON_TARGET))
        }
    }
}

@ConfigSerializable
data class StoredMessages(
    @Comment("Message that will be sent to players on join")
    val join: Component = text("Welcome to Drawstring!").append(text("Drawstring", color(0xFFCCFF))),

    @Comment("A style to apply to all command responses")
    val commandStyle: Style = style(color(0xCC22BB) as TextColor?),

    @Comment("Prefix for command responses")
    val commandPrefix: Component = Component.empty()
)
