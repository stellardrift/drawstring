/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring.module

import ca.stellardrift.drawstring.Module
import ca.stellardrift.drawstring.id
import ca.stellardrift.drawstring.joinTo
import ca.stellardrift.drawstring.key
import ca.stellardrift.drawstring.mixin.HungerManagerAccess
import ca.stellardrift.drawstring.unaryPlus
import io.github.ladysnake.pal.AbilitySource
import io.github.ladysnake.pal.Pal
import io.github.ladysnake.pal.PlayerAbility
import io.github.ladysnake.pal.VanillaAbilities
import net.minecraft.command.argument.EntityArgumentType.getPlayers
import net.minecraft.command.argument.EntityArgumentType.players
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.LiteralText
import net.minecraft.text.Text
import net.minecraft.util.Util

private const val MAX_FOOD = 20
private const val ARG_TARGET = "target"

class Abilities : Module() {
    override val name: String = "Abilities"

    val fly by command {
        requires { it.hasPermissionLevel(2) }
        then(
            (players() key ARG_TARGET).executes {
                toggleFlight(it.source, getPlayers(it, ARG_TARGET))
            }
        )
        executes { toggleFlight(it.source, setOf(it.source.player)) }
    }

    val god by command {
        requires { it.hasPermissionLevel(2) }
        then(
            (players() key ARG_TARGET).executes {
                toggleInvulnerability(it.source, getPlayers(it, ARG_TARGET))
            }
        )
        executes { toggleInvulnerability(it.source, setOf(it.source.player)) }
    }

    val invuln by command {
        redirect(god)
    }

    val heal by command {
        requires { it.hasPermissionLevel(2) }
        then(
            (players() key ARG_TARGET).executes {
                heal(it.source, getPlayers(it, ARG_TARGET))
            }
        )
        executes { heal(it.source, setOf(it.source.player)) }
    }

    private val commandAbility = Pal.getAbilitySource(id("command"))
    private val flightResponse = LiteralText("Flight toggled on: ")
    private val invulnResponse = LiteralText("Invulnerability toggled on: ")
    private val commaSep = LiteralText(", ")

    private fun AbilitySource.toggle(target: PlayerEntity, ability: PlayerAbility): Boolean {
        return if (grants(target, ability)) {
            revokeFrom(target, ability)
            false
        } else {
            grantTo(target, ability)
            true
        }
    }

    private fun heal(src: ServerCommandSource, targets: Collection<ServerPlayerEntity>): Int {
        targets.forEach {
            healPlayer(it)
            it.sendMessage(LiteralText("You've been healed"), false)
        }
        src.sendFeedback(LiteralText("${targets.size} players healed"), false)
        return targets.size
    }

    /**
     * Heal player and restore saturation to full, resetting exhaustion
     */
    private fun healPlayer(target: ServerPlayerEntity) {
        target.health = target.maxHealth
        target.hungerManager.foodLevel = MAX_FOOD
        val decoratedManager = target.hungerManager as HungerManagerAccess
        decoratedManager.setExhaustion(0f)
        decoratedManager.setFoodStarvationTimer(0)
        decoratedManager.setFoodSaturationLevel(MAX_FOOD.toFloat()) // saturation is capped at max food
    }

    private fun toggleFlight(src: ServerCommandSource, targets: Collection<ServerPlayerEntity>) =
        toggleAbility(
            src, targets, VanillaAbilities.ALLOW_FLYING,
            flightResponse
        ) { LiteralText("Flight status set to ").append(+it) }

    private fun toggleInvulnerability(src: ServerCommandSource, targets: Collection<ServerPlayerEntity>) =
        toggleAbility(
            src, targets, VanillaAbilities.INVULNERABLE,
            invulnResponse
        ) { LiteralText("Invulnerable status set to ").append(+it) }

    /**
     * Toggle an [ability] on [targets], sending [executorResponse] to the sender
     * and [targetResponseMaker] to each target.
     */
    private fun toggleAbility(
        src: ServerCommandSource,
        targets: Collection<ServerPlayerEntity>,
        ability: PlayerAbility,
        executorResponse: Text,
        targetResponseMaker: (self: Boolean) -> Text
    ): Int {
        val response = targets.asSequence()
            .map { it to commandAbility.toggle(it, ability) }
            .map { it.first.sendSystemMessage(targetResponseMaker(it.second), Util.NIL_UUID); it }
            .map { (target, result) -> LiteralText("").append(target.displayName).styled { (+result).style } }
            .joinTo(executorResponse, commaSep)

        if (targets.size > 1 || src.entity !in targets) { // only set self
            src.sendFeedback(response, true)
        }
        return targets.size
    }
}
