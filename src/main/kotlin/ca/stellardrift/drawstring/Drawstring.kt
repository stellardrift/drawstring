/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring

import ca.stellardrift.confabricate.Confabricate
import com.mojang.brigadier.arguments.ArgumentType
import io.leangen.geantyref.GenericTypeReflector.erase
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents
import net.fabricmc.loader.api.FabricLoader
import net.fabricmc.loader.api.ModContainer
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint
import net.kyori.adventure.platform.fabric.FabricServerAudiences
import net.kyori.adventure.serializer.configurate4.ConfigurateComponentSerializer
import net.kyori.adventure.text.minimessage.MiniMessage
import net.minecraft.server.command.CommandManager.argument
import net.minecraft.text.LiteralText
import net.minecraft.text.MutableText
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import net.minecraft.util.Identifier
import org.spongepowered.configurate.hocon.HoconConfigurationLoader
import org.spongepowered.configurate.kotlin.objectMapperFactory
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import java.nio.file.Files
import java.nio.file.Path

// Common utilities across modules

internal class PreLaunchInjections : PreLaunchEntrypoint {
    override fun onPreLaunch() {
        PreLaunchHacks.hackilyLoadForMixin("com.mojang.datafixers.kinds.K2")
        // Load configurate-dfu here too, to prevent cross-compat issues in dev
        PreLaunchHacks.hackilyLoadForMixin("org.spongepowered.configurate.extra.dfu.v4.NodeMaplike")
    }
}

internal const val MOD_ID = "drawstring"
const val ENTRY_MODULE = "$MOD_ID:module"

fun id(path: String) = Identifier(MOD_ID, path)

object Drawstring {
    private lateinit var container: ModContainer
    lateinit var mainConfig: Path private set
    private lateinit var modules: ModuleHandler
    private var _adventure: FabricServerAudiences? = null
    val adventure: FabricServerAudiences get() = requireNotNull(_adventure) { "Adventure can only be accessed with a running server!" }

    fun init() {
        val discoveredModules = FabricLoader.getInstance().getEntrypoints(ENTRY_MODULE, Module::class.java)

        ServerLifecycleEvents.SERVER_STARTED.register { this._adventure = FabricServerAudiences.of(it) }
        ServerLifecycleEvents.SERVER_STOPPED.register { this._adventure = null }

        container = FabricLoader.getInstance().getModContainer(MOD_ID)
            .orElseThrow { ExceptionInInitializerError("Expected mod id $MOD_ID does not have a mod present!") }

        // Load configurations
        mainConfig = Confabricate.configurationFile(container, true)
        val configDir = mainConfig.parent
        Files.createDirectories(configDir)

        // Create a reference that automatically reloads, with extra serializers
        val config = Confabricate.fileWatcher()
            .listenToConfiguration(
                {
                    HoconConfigurationLoader.builder()
                        .path(it)
                        .prettyPrinting(true)
                        .defaultOptions {
                            it.serializers {
                                it.register(
                                    { with(erase(it)) { kotlin.isData || isAnnotationPresent(ConfigSerializable::class.java) } },
                                    objectMapperFactory().asTypeSerializer()
                                ).registerAll(
                                    ConfigurateComponentSerializer.builder()
                                        .outputStringComponents(true)
                                        .scalarSerializer(MiniMessage.get())
                                        .build().serializers()
                                )
                            }
                        }
                        .build()
                },
                mainConfig
            )
        this.modules = ModuleHandler(config, discoveredModules.toSet())
        modules.initialize()
        this.modules.config.save()
    }

    fun <T : Module> module(type: Class<T>): T {
        return this.modules[type.kotlin] ?: throw IllegalArgumentException("Could not find module with type $type!")
    }
}

// Brigadier bits //

infix fun <T> ArgumentType<T>.key(key: String) = argument(key, this)

// Text //
private val LITERAL_TRUE = LiteralText("true").formatted(Formatting.GREEN)
private val LITERAL_FALSE = LiteralText("false").formatted(Formatting.RED)

fun <T : Text> Sequence<T>.join(separator: Text = LiteralText.EMPTY): MutableText {
    return joinTo(LiteralText.EMPTY, separator)
}

fun <T : Text> Sequence<T>.joinTo(starter: Text, separator: Text = LiteralText.EMPTY): MutableText {
    val acc = starter.shallowCopy()
    val it = iterator()
    while (it.hasNext()) {
        acc.append(it.next())
        if (it.hasNext()) {
            acc.append(separator)
        }
    }
    return acc
}

operator fun Boolean.unaryPlus(): Text = if (this) {
    LITERAL_TRUE
} else {
    LITERAL_FALSE
}
