/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring

import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import com.mojang.brigadier.tree.CommandNode
import com.mojang.brigadier.tree.LiteralCommandNode
import io.leangen.geantyref.TypeToken
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback
import net.kyori.adventure.audience.Audience
import net.kyori.adventure.platform.fabric.AdventureCommandSourceStack
import net.minecraft.server.command.CommandManager.literal
import net.minecraft.server.command.CommandOutput
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.server.network.ServerPlayerEntity
import org.spongepowered.configurate.CommentedConfigurationNode
import org.spongepowered.configurate.NodePath.path
import org.spongepowered.configurate.reference.ConfigurationReference
import org.spongepowered.configurate.reference.ValueReference
import org.spongepowered.configurate.transformation.ConfigurationTransformation
import java.lang.IllegalStateException
import java.lang.reflect.Type
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.reflect.jvm.javaType
import kotlin.reflect.jvm.jvmErasure

typealias CommandMaker = LiteralArgumentBuilder<ServerCommandSource>.() -> Unit

abstract class Module {

    internal val commands: MutableSet<CommandProperty> = mutableSetOf()
    internal val configOptions: MutableSet<ConfigProperty<*>> = mutableSetOf()
    internal val moduleReferences: MutableSet<ModuleProperty<*>> = mutableSetOf()

    abstract val name: String

    protected fun command(maker: CommandMaker): CommandProperty {
        return CommandProperty(maker).apply { commands.add(this) }
    }

    protected fun <V> config(transformation: ConfigurationTransformation? = null): ConfigProperty<V> {
        return ConfigProperty<V>(transformation).apply { configOptions.add(this) }
    }

    protected fun <V : Module> module(): ModuleProperty<V> {
        return ModuleProperty<V>().also { moduleReferences.add(it) }
    }

    // Adventure helpers

    fun CommandOutput.audience(): Audience {
        return Drawstring.adventure.audience(this)
    }

    fun ServerCommandSource.audience(): AdventureCommandSourceStack {
        return Drawstring.adventure.audience(this)
    }

    fun Iterable<ServerPlayerEntity>.audience(): Audience {
        return Drawstring.adventure.audience(this)
    }

    /* An opportunity to perform any other sort of initialization */
    open fun initialize() {
    }
}

/**
 * A property managing a configuration section.
 */
class ConfigProperty<V> internal constructor(internal val transform: ConfigurationTransformation?) : ReadWriteProperty<Module, V> {
    lateinit var name: String private set
    lateinit var type: Type private set
    lateinit var ref: ValueReference<V, *> private set

    operator fun provideDelegate(self: Module, property: KProperty<*>): ReadWriteProperty<Module, V> {
        this.name = property.name
        this.type = property.returnType.javaType
        return this
    }

    internal fun init(ref: ValueReference<*, CommentedConfigurationNode>) {
        if (this::ref.isInitialized) {
            throw IllegalStateException("Value must only be initialized once")
        }
        @Suppress("UNCHECKED_CAST")
        this.ref = ref as ValueReference<V, *>
    }

    override fun getValue(thisRef: Module, property: KProperty<*>): V {
        return ref.get()!!
    }

    override fun setValue(thisRef: Module, property: KProperty<*>, value: V) {
        this.ref.setAndSaveAsync(value)
    }

    fun update(updater: (V) -> V) {
        this.ref.updateAsync(updater)
    }
}

class CommandProperty internal constructor(private val maker: CommandMaker) : ReadOnlyProperty<Module, CommandNode<ServerCommandSource>> {
    lateinit var name: String private set
    private lateinit var registered: LiteralCommandNode<ServerCommandSource> private set

    operator fun provideDelegate(self: Module, prop: KProperty<*>): ReadOnlyProperty<Module, CommandNode<ServerCommandSource>> {
        this.name = prop.name
        return this
    }

    override fun getValue(thisRef: Module, property: KProperty<*>): CommandNode<ServerCommandSource> {
        return this.registered
    }

    internal fun register(manager: CommandDispatcher<ServerCommandSource>) {
        registered = manager.register(literal(this.name).apply(maker))
    }
}

/**
 * A property that refers to another module in the layout.
 */
class ModuleProperty<T : Module> internal constructor() : ReadOnlyProperty<Module, T> {
    private lateinit var expectedType: KClass<T>
    lateinit var expected: T

    operator fun provideDelegate(self: Module, prop: KProperty<*>): ReadOnlyProperty<Module, T> {
        @Suppress("UNCHECKED_CAST")
        expectedType = prop.returnType.jvmErasure as KClass<T>
        return this
    }

    override fun getValue(thisRef: Module, property: KProperty<*>): T {
        return expected
    }

    internal fun init(handler: ModuleHandler) {
        expected = handler[expectedType] ?: throw IllegalArgumentException("Module required $expectedType but no such module could be found")
    }
}

/**
 * A registry for modules
 */
class ModuleHandler(internal val config: ConfigurationReference<CommentedConfigurationNode>, val modules: Set<Module>) : Iterable<Module> {
    fun initialize() {

        // Prepare commands
        CommandRegistrationCallback.EVENT.register(
            CommandRegistrationCallback { manager, _ ->
                for (module in this.modules) {
                    for (command in module.commands) {
                        command.register(manager)
                    }
                }
            }
        )

        for (module in this.modules) {
            // Load configuration (this will autoreload)
            val path = path("modules", module.name)
            if (module.configOptions.size == 1) { // single root node
                val option = module.configOptions.first()
                if (option.transform != null) {
                    option.transform.apply(config[path])
                }
                option.init(config.referenceTo(TypeToken.get(option.type), path))
            } else {
                // each config option is named
                module.configOptions.forEach {
                    val childPath = path.withAppendedChild(it.name)
                    if (it.transform != null) {
                        it.transform.apply(config[childPath])
                    }
                    it.init(config.referenceTo(TypeToken.get(it.type), childPath))
                }
            }

            // Set up other module references (TODO: initialization order based on this?)
            module.moduleReferences.forEach {
                it.init(this)
            }

            // Call initializer
            module.initialize()
        }
    }

    operator fun <T : Any> get(module: KClass<T>): T? {
        @Suppress("UNCHECKED_CAST")
        return this.modules.find { module.isInstance(it) } as T?
    }

    override fun iterator(): Iterator<Module> {
        return modules.iterator()
    }
}
