/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring.mixin.server.entity;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
public abstract class EntityMixin {

    @Shadow public abstract boolean isAlive();

    /**
     * Prevent dupes by dropping copies of itemstacks, and setting the count of original items to 0.
     *
     * Based on the fix in https://github.com/PaperMC/Paper/blob/1ab021ddca9dc2bd48ce4308cccb7440528af25a/Spigot-Server-Patches/0463-Fix-numerous-item-duplication-issues-and-teleport-is.patch
     */
    @ModifyArg(method = "dropStack(Lnet/minecraft/item/ItemStack;F)Lnet/minecraft/entity/ItemEntity;",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/ItemEntity;<init>(Lnet/minecraft/world/World;DDDLnet/minecraft/item/ItemStack;)V"), index = 4)
    private ItemStack guardDroppedStack(final ItemStack input) {
        final ItemStack copy = input.copy();
        input.setCount(0);
        return copy;
    }

    @Inject(method = "canUsePortals", at = @At("HEAD"), cancellable = true)
    private void canOnlyPortalIfAlive(final CallbackInfoReturnable<Boolean> ci) {
        // Don't allow portaling if the entity is dead
        if (!this.isAlive()) {
            ci.setReturnValue(false);
        }
    }

}
