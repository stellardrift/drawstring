/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring.mixin.server.command;

import ca.stellardrift.drawstring.Drawstring;
import ca.stellardrift.drawstring.module.GameMessages;
import net.kyori.adventure.platform.fabric.FabricAudiences;
import net.kyori.adventure.platform.fabric.impl.mixin.CommandSourceStackMixin;
import net.kyori.adventure.text.Component;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Dynamic;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Group;
import org.spongepowered.asm.mixin.injection.ModifyArg;

@Mixin(value = ServerCommandSource.class, priority = 1001) // raised priority to inject into adventure-added methods
public class ServerCommandSourceMixin {

    @ModifyArg(method = "sendFeedback", at = @At(value = "INVOKE", target = "Lnet/minecraft/server/command/CommandOutput;sendSystemMessage(Lnet/minecraft/text/Text;Ljava/util/UUID;)V"), index = 0)
    private Text drawstring$modifyOutput(final Text input) {
        return FabricAudiences.update(input, original -> Drawstring.INSTANCE.module(GameMessages.class).transformCommandSourceResponse(original));
    }


    // Development
    @Group(name = "adventureSendSuccess", min = 1, max = 1)
    @Dynamic(mixin = CommandSourceStackMixin.class)
    @ModifyArg(method = "sendSuccess", at = @At(value = "INVOKE", target = "Lnet/minecraft/server/command/ServerCommandSource;sendMessage(Lnet/kyori/adventure/identity/Identity;Lnet/kyori/adventure/text/Component;)V"), remap = false)
    private Component drawstring$modifyAdventureOutput(final Component input) {
        return Drawstring.INSTANCE.module(GameMessages.class).transformCommandSourceResponse(input);
    }

    // Production
    // TODO: would be nice to be able to do this automatically
    @Group(name = "adventureSendSuccess")
    @Dynamic(mixin = CommandSourceStackMixin.class)
    @ModifyArg(method = "sendSuccess", at = @At(value = "INVOKE", target = "Lnet/minecraft/class_2168;sendMessage(Lnet/kyori/adventure/identity/Identity;Lnet/kyori/adventure/text/Component;)V"), remap = false)
    private Component drawstring$modifyAdventureOutput$Prod(final Component input) {
        // redirect to primary method
        return this.drawstring$modifyAdventureOutput(input);
    }
}
