/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring.mixin.datafixers;

import com.mojang.datafixers.DataFixUtils;
import com.mojang.datafixers.DataFixerBuilder;
import com.mojang.datafixers.schemas.Schema;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Collections;
import java.util.Set;

@Mixin(DataFixerBuilder.class)
public class DataFixerBuilderMixin {

    // Apply paper "Cache DataFixerUpper Rewrite Rules on demand" patch
    // https://github.com/PaperMC/Paper/blob/7ae47d4eb33fd7b2baf6c80a95afa4ff1c1d15e3/Spigot-Server-Patches/0571-Cache-DataFixerUpper-Rewrite-Rules-on-demand.patch

    private int drawstring$minDataFixPrecacheVersion;

    @Inject(method = "<init>", at = @At("RETURN"))
    private void drawstring$initializePrecache(final int dataVersion, final CallbackInfo ci) {
        this.drawstring$minDataFixPrecacheVersion = Integer.getInteger("drawstring.minPrecachedDatafixVersion", dataVersion + 1);
    }

    @Redirect(method = "build", at = @At(value = "INVOKE", target = "Lcom/mojang/datafixers/schemas/Schema;types()Ljava/util/Set;"), remap = false)
    private Set<String> drawstring$noTypeNamesIfBelowPrecacheVersion(final Schema schema) {
        if (DataFixUtils.getVersion(schema.getVersionKey()) < this.drawstring$minDataFixPrecacheVersion) {
            return Collections.emptySet();
        } else {
            return schema.types();
        }
    }
}
