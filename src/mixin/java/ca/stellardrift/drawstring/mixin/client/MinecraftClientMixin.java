/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring.mixin.client;

import ca.stellardrift.drawstring.StoredWindowData;
import ca.stellardrift.drawstring.StoredWindowDataKt;
import ca.stellardrift.drawstring.WindowSettingsBridge;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.WindowSettings;
import net.minecraft.client.sound.SoundManager;
import net.minecraft.client.util.Window;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MinecraftClient.class)
public class MinecraftClientMixin {
    @Shadow @Final private SoundManager soundManager;
    @Shadow @Final private Window window;
    private boolean drawstring$agressiveWindowSettings;

    // Pause sound when inactive

    @Inject(method = "onWindowFocusChanged", at = @At("RETURN"))
    private void toggleFocused(final boolean focused, final CallbackInfo ci) {
        if (this.soundManager != null) {
            if (focused) {
                this.soundManager.resumeAll();
            } else {
                this.soundManager.pauseAll();
            }
        }
    }

    // Preserve window layout

    @ModifyArg(method = "<init>", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/util/WindowProvider;createWindow(Lnet/minecraft/client/WindowSettings;Ljava/lang/String;Ljava/lang/String;)Lnet/minecraft/client/util/Window;"))
    private WindowSettings injectSettings(final WindowSettings original) {
        final StoredWindowData data = StoredWindowDataKt.loadWindowData();
        this.drawstring$agressiveWindowSettings = data.getAgressive();
        if (original.height == -1 || original.width == -1 || data.getAgressive()) {
            final WindowSettings ret = new WindowSettings(
                    original.width == -1 || data.getAgressive() ? data.getWidth() : original.width,
                    original.height == -1 || data.getAgressive() ? data.getHeight() : original.height,
                    original.fullscreenWidth,
                    original.fullscreenHeight,
                    false);

            final WindowSettingsBridge $ret = (WindowSettingsBridge) ret;
            $ret.setStartX(data.getStartX());
            $ret.setStartY(data.getStartY());
            return ret;
        } else {
            return original;
        }
    }

    @Inject(method = "close", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/util/Window;close()V"))
    private void handleClose(final CallbackInfo ci) {
        final Window window = this.window;
        final StoredWindowData data = new StoredWindowData(this.drawstring$agressiveWindowSettings, window.getWidth(), window.getHeight(), window.getX(), window.getY());
        StoredWindowDataKt.saveWindowData(data);
    }
}
