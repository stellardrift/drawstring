/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring.mixin.client;

import ca.stellardrift.drawstring.WindowSettingsBridge;
import net.minecraft.client.WindowSettings;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(WindowSettings.class)
public class WindowSettingsMixin implements WindowSettingsBridge {
    private int drawstring$startX = -1;
    private int drawstring$startY = -1;
    @Override
    public int getStartX() {
        return this.drawstring$startX;
    }

    @Override
    public void setStartX(int startX) {
        this.drawstring$startX = startX;
    }

    @Override
    public int getStartY() {
        return this.drawstring$startY;
    }

    @Override
    public void setStartY(int startY) {
        this.drawstring$startY = startY;
    }
}
