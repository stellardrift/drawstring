/*
 * Copyright (C) zml and other contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.stellardrift.drawstring.mixin.client.util;

import ca.stellardrift.drawstring.WindowSettingsBridge;
import net.minecraft.client.WindowEventHandler;
import net.minecraft.client.WindowSettings;
import net.minecraft.client.util.MonitorTracker;
import net.minecraft.client.util.Window;
import org.lwjgl.glfw.GLFW;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Window.class)
public class WindowMixin {

    @Shadow @Final private long handle;
    @Shadow private int windowedY;
    @Shadow private int windowedX;
    @Shadow private int x;
    @Shadow private int y;

    // this only works on fabric's end. to do this on normal mixin, would need custom injection point
    // we know `this.handle` has already been initialized at this point, and so have windowedX, windowedY, x, and y
    @Inject(method = "<init>", at = @At(value = "FIELD",target = "Lnet/minecraft/client/util/Window;windowedY:I", opcode = Opcodes.PUTFIELD, shift = At.Shift.AFTER), require = 2, expect = 2, slice = @Slice(
            from = @At(value = "INVOKE", target = "Lorg/lwjgl/glfw/GLFW;glfwCreateWindow(IILjava/lang/CharSequence;JJ)J", remap = false),
            to = @At(value = "INVOKE", target = "Lorg/lwjgl/glfw/GLFW;glfwMakeContextCurrent(J)V", remap = false)
    ))
    private void drawstring$restoreWindowPosition(final WindowEventHandler eh, final MonitorTracker mt, final WindowSettings settings, final String videoMode, final String title, final CallbackInfo ci) {
        // TODO: figure out if it's possible to make sure the window bounds are actually on the current display
        // otherwise you get lost windows :p
        final WindowSettingsBridge $settings = (WindowSettingsBridge) settings;
        final int x = $settings.getStartX();
        final int y = $settings.getStartY();
        if (x != -1) {
            this.windowedX = this.x = x;
        }
        if (y != -1) {
            this.windowedY = this.y = y;
        }
    }
}
